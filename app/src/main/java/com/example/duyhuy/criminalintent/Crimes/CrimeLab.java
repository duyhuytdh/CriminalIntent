package com.example.duyhuy.criminalintent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Duy Huy on 15/01/2016.
 */
public class CrimeLab {

    private static  CrimeLab sCrimeLab;
//    private List<Crime> mCrimes;
    private Context mContext;
    private SQLiteDatabase mDataBase;


    public  static  CrimeLab get(Context context){
        if(sCrimeLab==null){
            sCrimeLab = new CrimeLab(context);
        }
        return  sCrimeLab;
    }

    private CrimeLab(Context context){
        mContext = context.getApplicationContext();
        mDataBase = new CrimeBaseHelper(mContext)   .getWritableDatabase();
//        mCrimes = new ArrayList<>();
    }
    public List<Crime> getmCrimes(){
//        return  mCrimes;
        List<Crime> crimes = new ArrayList<>();
        CrimeCursorWrapper cursor = queryCrimes(null,null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                crimes.add(cursor.getCrime());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return crimes;
   }

    public  Crime getCrime(UUID id){
//        for(Crime crime : mCrimes){
//            if(crime.getmId().equals(id))
//            {
//                return  crime;
//            }
//        }

        CrimeCursorWrapper cursor = queryCrimes(CrimeDBSchema.CrimeTable.Cols.UUID +"=?",new String[]{id.toString()});

        try{
            if(cursor.getCount() == 0){
                return null;
            }
            cursor.moveToFirst()    ;
            return cursor.getCrime();
        } finally {
            cursor.close();
        }
    }

    public void addCrime(Crime crime){
//        mCrimes.add(crime);
        ContentValues values = getContentValue(crime);

        mDataBase.insert(CrimeDBSchema.CrimeTable.NAME,null, values);
    }
    public void deleteCrime(UUID id){
        //int current = 1;
//        for(int i=0; i< mCrimes.size(); i++){
//            if(mCrimes.get(i).getmId()==id){
//                //current = i;
//                mCrimes.remove(i);
//                return;
//            }
//        }

        mDataBase.delete(
                  CrimeDBSchema.CrimeTable.NAME
                , CrimeDBSchema.CrimeTable.Cols.UUID +"=?"
                , new String[]{id.toString()}
        );
    }

    public static ContentValues getContentValue(Crime crime) {
        ContentValues values = new ContentValues();
        values.put(CrimeDBSchema.CrimeTable.Cols.UUID, crime.getmId().toString());
        values.put(CrimeDBSchema.CrimeTable.Cols.TITLE, crime.getmTitle());
        values.put(CrimeDBSchema.CrimeTable.Cols.DATE, crime.getmDate().getTime());
        values.put(CrimeDBSchema.CrimeTable.Cols.SOLVED, crime.ismSolved() ? 1:0);
        return values;
    }

    public void updateCrime(Crime crime){
        String str_uuid = crime.getmId().toString();
        ContentValues values = getContentValue(crime);

        mDataBase.update(
                CrimeDBSchema.CrimeTable.NAME
                , values
                , CrimeDBSchema.CrimeTable.Cols.UUID + "=?"
                , new String[]{str_uuid}
        );
    }

    private CrimeCursorWrapper queryCrimes(String whereClause, String[] whereArgs){
        Cursor cursor = mDataBase.query(
                CrimeDBSchema.CrimeTable.NAME
                ,null //Columns - null selects all columns
                ,whereClause
                ,whereArgs
                ,null //Group By
                ,null //having
                ,null //orderBy
        );
        return new CrimeCursorWrapper(cursor);
    }

}
