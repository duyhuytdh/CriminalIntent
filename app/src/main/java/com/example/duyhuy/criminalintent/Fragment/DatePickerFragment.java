package com.example.duyhuy.criminalintent;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Duy Huy on 24/01/2016.
 */
public class DatePickerFragment extends DialogFragment {

    private static final String AGR_DATE = "date";
    public static final String EXTRA_DATE = "com.example.duyhuy.criminalintent.date";

    private DatePicker mDatePicker;

    public static DatePickerFragment newInstance(Date date){
        Bundle args = new Bundle();
        args.putSerializable(AGR_DATE, date);

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void sendResult(int ResultCode, Date date){
        if (getTargetFragment() == null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_DATE, date);

        getTargetFragment().onActivityResult(getTargetRequestCode(), ResultCode, intent);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Date date = (Date) getArguments().getSerializable(AGR_DATE);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_date, null);

        mDatePicker = (DatePicker)v.findViewById(R.id.dialog_date_picker_view);
        mDatePicker.init(year, month, day, null);

        return new AlertDialog.Builder(getActivity())
                                .setView(v)
                                .setTitle(R.string.date_picker_title)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        int year = mDatePicker.getYear();
                                        int month = mDatePicker.getMonth();
                                        int day = mDatePicker.getDayOfMonth();
                                        Date date = new GregorianCalendar(year, month, day).getTime();
                                        Log.d("huytd", "Date Picker: "+ date.toString());
                                        sendResult(Activity.RESULT_OK, date);
                                    }
                                })
                                .create();
    }

}
