package com.example.duyhuy.criminalintent;

import android.support.v4.app.Fragment;

/**
 * Created by Duy Huy on 17/01/2016.
 */
public class CrimeListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }
}
