package com.example.duyhuy.criminalintent;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Duy Huy on 17/01/2016.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity {
    protected abstract Fragment createFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        android.support.v4.app.FragmentManager frm = getSupportFragmentManager();
        Fragment fragment = frm.findFragmentById(R.id.fragment_container);
        if(fragment==null){
            fragment = createFragment();
            frm.beginTransaction().add(R.id.fragment_container,fragment).commit();
        }

    }
}
